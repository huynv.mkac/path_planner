#!/usr/bin/env python

import rospy
from mkac_planner import MkacPlanner
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Pose, Point, Quaternion
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance, PoseStamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler, euler_from_quaternion
from move_base_msgs.msg import MoveBaseGoal
from tf import TransformListener
from ultils import *

class PublishMkacPath():
    def __init__(self):
        rospy.init_node("mkac_planner", log_level=rospy.DEBUG)
        rospy.loginfo("Start mkac_planner")

        rospy.on_shutdown(self.shutdown)
        self.rate = rospy.Rate(10)

        self.pub_plan = rospy.Publisher('mkac_path_plan', Path, queue_size=10)
        self.movebase_goal_sub = rospy.Subscriber('/move_base_simple/goal', PoseStamped, self.moveBaseGoalCB)
        rospy.Subscriber('/odom', Odometry, self.updateOdom)
        rospy.Subscriber('/robot_pose', Pose, self.robotPoseCB)

        self.frame_id = 'map'
        self.currentPose = RBPose()
        self.start = RBPose()
        self.goal = RBPose()
        self.tf_listener = TransformListener()
        while not rospy.is_shutdown():
            self.rate.sleep()

    def shutdown(self):
        rospy.loginfo("Shutting down [{}] node".format(rospy.get_name()))

    def robotPoseCB(self, msg):
        self.currentPose.x = msg.position.x
        self.currentPose.y = msg.position.y
        quaternion = (
                        msg.orientation.x,
                        msg.orientation.y,
                        msg.orientation.z,
                        msg.orientation.w)
        euler = euler_from_quaternion(quaternion)
        self.currentPose.phi = euler[2]

    def moveBaseGoalCB(self, msg):
        # rospy.loginfo("move_base/goal: {}".format(msg))
        self.goal.x = msg.pose.position.x
        self.goal.y = msg.pose.position.y
        quaternion = (
                        msg.pose.orientation.x,
                        msg.pose.orientation.y,
                        msg.pose.orientation.z,
                        msg.pose.orientation.w)
        euler = euler_from_quaternion(quaternion)
        self.goal.phi = euler[2]
        self.publishPlan(self.currentPose, self.goal)

    def updateOdom(self, msg):
        ''' Problem: chua tim duoc cach chuyen toa do tu /odom sang /map

        '''
        pass
        # odom_pose = PoseStamped()
        # odom_pose.header = msg.header
        # odom_pose.pose = msg.pose.pose
        # pose_out = self.tf_listener.transformPose('/map', odom_pose)
        # rospy.loginfo("pose_out: {}".format(pose_out))
        # tran, rot = self.tf_listener.lookupTransform('/map', '/odom', rospy.Time(0))
        # self.currentPose.x = msg.pose.pose.position.x
        # self.currentPose.y = msg.pose.pose.position.y
        # quaternion = (
        #                 msg.pose.pose.orientation.x,
        #                 msg.pose.pose.orientation.y,
        #                 msg.pose.pose.orientation.z,
        #                 msg.pose.pose.orientation.w)
        # euler = euler_from_quaternion(quaternion)
        # self.currentPose.phi = euler[2]
        # rospy.loginfo("{}".format([self.currentPose.x,
                                # self.currentPose.y, self.currentPose.phi]))

    def publishPlan(self, start, goal):
        rospy.loginfo("Start: {} - Stop: {}".format([start.x, start.y, start.phi], [goal.x, goal.y, goal.phi]))
        msg = Path()
        msg.header.frame_id = self.frame_id
        msg.header.stamp = rospy.Time.now()
        Path_XYRad = MkacPlanner(start, goal, Rmax=1)
        for wp in Path_XYRad:
            pose = PoseStamped()
            pose.pose.position.x = wp.x
            pose.pose.position.y = wp.y
            pose.pose.position.z = 0
            quat = quaternion_from_euler(0, 0, wp.phi)
            pose.pose.orientation.x = quat[0]
            pose.pose.orientation.y = quat[1]
            pose.pose.orientation.z = quat[2]
            pose.pose.orientation.w = quat[3]
            msg.poses.append(pose)
        self.pub_plan.publish(msg)
        rospy.loginfo("Publish plan")


if __name__ == "__main__":
    publishMkacPath = PublishMkacPath()
    # A = RBPose(1.56, 0, 3.14)
    # B = RBPose(-7.5, -5.5, -1.57)
    # publishMkacPath.publishPlan(A, B)