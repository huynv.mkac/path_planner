#!/usr/bin/env python3
# import numpy as np
import matplotlib.pyplot as plt
import math
from geometry_msgs.msg import Pose, Point, Quaternion
from ultils import *

def MkacPlanner(A, B, Rmax=5, Rmin=0.5, step = 0.2):
    """MkacPlanner(A, B, Rmax, Rmin, step): Creat path from A to B

    Args:
        A (RBPose): Pose Start
        B (RBPose): Pose Stop
        Rmax (float, optional): Radius max of circle. Defaults to 5.
        Rmin (float, optional): Min Radius of circle. Defaults to 0.5.
        step (float, optional): distance step. Defaults to 0.2.

    Returns:
        X, Y: List of coordinates points in path
    """
    # B1: Phi
    Phi = abs(A.phi - B.phi)
    if(Phi > math.pi):
        Phi = 2*math.pi - Phi
    # print("Phi: {}".format(Phi))

    # B2: Find I(xI, yI)
    I = Point()
    I = findI(A, B)
    # print("I: {}".format([I.x, I.y]))

    # B3: dmin
    d_min = min(distance(A,I), distance(B,I))
    # print("d_min: {}".format(d_min))

    # B4: R
    R = (d_min*math.tan(Phi/2))/2
    # print("R: {}".format(R))
    R = R if R < Rmax else Rmax
    R = R if R > Rmin else Rmin

    # B5: Kiem tra dieu kien
    vector_AI = [I.x - A.x, I.y - A.y]
    vector_A = [math.cos(A.phi), math.sin(A.phi)]
    vector_BI = [I.x - B.x, I.y - B.y]
    vector_B = [math.cos(B.phi), math.sin(B.phi)]
    cos_AI_A = cos2Vector(vector_AI, vector_A)
    cos_IB_B = cos2Vector(vector_BI, vector_B)
    if(cos_AI_A*cos_IB_B > 0):
        print("StartPose, StopPose invalid")
        return []

    # B6: Tim diem Ia, Ib (Diem bat dau va diem ket thuc cung tron)
    # Tim Ia:
    #   + Ia thuoc duong thang qua vecto A
    #   + |IIa| tinh duoc
    #   + cos(vector(IIa), vector(IA)) = 1
    Ia = findIx(A, R, Phi, I)
    # print("Ia: {}".format([Ia.x, Ia.y]))
    Ib = findIx(B, R, Phi, I)
    # print("Ib: {}".format([Ib.x, Ib.y]))

    # -------------------------------------
    # B7: Chia buoc ve cac diem tren path
    # S1: A-> Ia: straight
    Path1 = posesStepStraight(Point(A.x, A.y), A, Ia, step)
    # S2: Ia-> Ib: curve
    Path2 = posesStepCircle(Ia, A, Ib, B, I, R, Phi, step)
    # S3: Ib->B: straight
    Path3 = posesStepStraight(Ib, B, Point(B.x, B.y), step)
    Path = Path1 + Path2 + Path3
    print("Path 1 created have {} poses".format(len(Path1)))
    print("Path 2 created have {} poses".format(len(Path2)))
    print("Path 3 created have {} poses".format(len(Path3)))
    print("Path created have {} poses".format(len(Path)))
    return Path

def MkacPlannerWithPoseStamped(start, stop, Rmax=5, Rmin=0.5, step = 0.2):
    # start, stop: PoseStamped
    pass