#!/usr/bin/env python3
# import numpy as np
import sys
import matplotlib.pyplot as plt
import math
from ultils import *
from mkac_planner import MkacPlanner

def test1():
    Rmax = 2
    Rmin = 0.5
    Xmin, Xmax = -1, 10
    Ymin, Ymax = -1, 10
    step = 0.5

    NumTestcase = 6
    m, n = 2, 3
    fig, ax = plt.subplots(m,n)
    for i in range(m):
        for j in range(n):
            testcase = i*n + j
            if testcase is 0:
                A = RBPose(0,0,0*math.pi)
                B = RBPose(8,6,math.pi/2.8)
                Xmin, Xmax = -1, 10
                Ymin, Ymax = -1, 10
            elif testcase is 1:
                A = RBPose(0,6,0*math.pi)
                B = RBPose(8,0, - math.pi/2.8)
                Xmin, Xmax = -1, 10
                Ymin, Ymax = -1, 10
            elif testcase is 2:
                A = RBPose(8,0,math.pi/2)
                B = RBPose(4,6,math.pi*4/3)
                Xmin, Xmax = -1, 10
                Ymin, Ymax = -1, 10
            elif testcase is 3:
                A = RBPose(8,0,-1.57)
                B = RBPose(4,6,math.pi*1/3)
                Xmin, Xmax = -1, 10
                Ymin, Ymax = -1, 10
            elif testcase is 4:
                A = RBPose(1.56, 0, 0)
                B = RBPose(-7.5, -5.5, 1.57)
                Xmin, Xmax = -10, 5
                Ymin, Ymax = -10, 5
            elif testcase is 5:
                A = RBPose(1.56, 0, 3.14)
                B = RBPose(-7.5, -5.5, -1.57)
                Xmin, Xmax = -10, 5
                Ymin, Ymax = -10, 5
            elif testcase is 6:
                A = RBPose(1.56, 0, 0)
                B = RBPose(-7.5, -5.5, 1.57)
                Xmin, Xmax = -10, 5
                Ymin, Ymax = -10, 5
            else:
                print("Testcase invalid!")
            print("\n******* Test case: {} **********".format(testcase))
            print("A: {}, B: {}".format([A.x,A.y,A.phi], [B.x,B.y,B.phi]))

            # B1: Phi
            Phi = abs(A.phi - B.phi)
            if(Phi > math.pi):
                Phi = 2*math.pi - Phi
            print("Phi: {}".format(Phi))

            # Show vector start, stop
            X = [A.x, B.x]
            Y = [A.y, B.y]
            U = [2*math.cos(A.phi), 2*math.cos(B.phi)]
            V = [2*math.sin(A.phi), 2*math.sin(B.phi)]
            q = ax[i, j].quiver(X, Y, U, V,units='xy' ,scale=1, color=['#0146ff', 'r'])
            ax[i, j].scatter(X, Y)
            ax[i, j].axline((A.x,A.y), (A.x + math.cos(A.phi), A.y + math.sin(A.phi)), ls='dashed')
            ax[i, j].axline((B.x,B.y), (B.x + math.cos(B.phi), B.y + math.sin(B.phi)), ls='dashed')


            # Line include A, B
            # Line: ax + by +x = 0

            # lineAA = lineFromPose(A)
            # print("lineAA: a: {}, b: {}, c: {}".format(lineAA.a, lineAA.b, lineAA.c))
            # lineBB = lineFromPose(B)
            # print("lineBB: a: {}, b: {}, c: {}".format(lineBB.a, lineBB.b, lineBB.c))

            # B2: Find I(xI, yI)
            I = Point()
            I = findI(A, B)
            print("I: {}".format([I.x, I.y]))
            ax[i, j].scatter(I.x, I.y, c='#8c564b')

            # B3: dmin
            d_min = min(distance(A,I), distance(B,I))
            print("d_min: {}".format(d_min))

            # B4: R
            R = (d_min*math.tan(Phi/2))/2
            print("R calculated: {}".format(R))
            R = R if R < Rmax else Rmax
            R = R if R > Rmin else Rmin
            print("R: {}".format(R))

            # B5: Kiem tra dieu kien
            # vector_AI = [I.x - A.x, I.y - A.y]
            # vector_A = [math.cos(A.phi), math.sin(A.phi)]
            # vector_BI = [I.x - B.x, I.y - B.y]
            # vector_B = [math.cos(B.phi), math.sin(B.phi)]
            # cos_AI_A = cos2Vector(vector_AI, vector_A)
            # cos_IB_B = cos2Vector(vector_BI, vector_B)
            # if(cos_AI_A*cos_IB_B > 0):
            #     print("A, B invalid")
            #     exit()

            # B6: Tim diem Ia, Ib (Diem bat dau va diem ket thuc cung tron)
            # Tim Ia:
            #   + Ia thuoc duong thang qua vecto A
            #   + |IIa| tinh duoc
            #   + cos(vector(IIa), vector(IA)) = 1
            Ia = findIx(A, R, Phi, I)
            print("Ia: {}".format([Ia.x, Ia.y]))
            Ib = findIx(B, R, Phi, I)
            print("Ib: {}".format([Ib.x, Ib.y]))
            ax[i, j].scatter(Ia.x, Ia.y, color='#8cff00', marker='o')
            ax[i, j].scatter(Ib.x, Ib.y, color='#8cff00')

            # -------------------------------------
            # B7: Chia buoc ve cac diem tren path

            Path = MkacPlanner(A, B, Rmax, Rmin, step)
            # ax[i, j].scatter(X, Y, color='g', s=10)
            # show path
            X = []
            Y = []
            U = []
            V = []
            for pose in Path:
                X.append(pose.x)
                Y.append(pose.y)
                U.append(math.cos(pose.phi))
                V.append(math.sin(pose.phi))
            # print("Number of pose in Path: {}".format(len(X)))
            p = ax[i, j].quiver(X, Y, U, V, units='xy', scale=0.51 , color='g') #
            ax[i, j].scatter(X, Y, color='g', s=10)

            # # show plot
            ax[i, j].set_aspect('equal')
            # ax[i, j].axline((1,1), (2,2))
            # ax[i, j].xlim(Xmin,Xmax)
            # ax[i, j].ylim(Ymin,Ymax)
            ax[i, j].grid()
            ax[i, j].set_title("Testcase: {}".format(testcase))
    plt.show()

def test_invidial():
    Rmax = 2
    Rmin = 0.5
    Xmin, Xmax = -1, 10
    Ymin, Ymax = -1, 10
    step = 0.2

    B = RBPose(1.56, 0, 3.14)
    A = RBPose(-7.5, -5.5, -1.57*1/2)
    Xmin, Xmax = -10, 5
    Ymin, Ymax = -10, 5

    fig, ax = plt.subplots(1)
    Xmin, Xmax = -10, 5
    Ymin, Ymax = -10, 5
    print("A: {}, B: {}".format([A.x,A.y,A.phi], [B.x,B.y,B.phi]))

    # B1: Phi
    Phi = abs(A.phi - B.phi)
    if(Phi > math.pi):
        Phi = 2*math.pi - Phi
    print("Phi: {}".format(Phi))

    # Show vector start, stop
    X = [A.x, B.x]
    Y = [A.y, B.y]
    U = [2*math.cos(A.phi), 2*math.cos(B.phi)]
    V = [2*math.sin(A.phi), 2*math.sin(B.phi)]
    q = ax.quiver(X, Y, U, V,units='xy' ,scale=1, color=['#0146ff', 'r'])
    ax.scatter(X, Y)
    ax.axline((A.x,A.y), (A.x + math.cos(A.phi), A.y + math.sin(A.phi)), ls='dashed')
    ax.axline((B.x,B.y), (B.x + math.cos(B.phi), B.y + math.sin(B.phi)), ls='dashed')


    # Line include A, B
    # Line: ax + by +x = 0

    # lineAA = lineFromPose(A)
    # print("lineAA: a: {}, b: {}, c: {}".format(lineAA.a, lineAA.b, lineAA.c))
    # lineBB = lineFromPose(B)
    # print("lineBB: a: {}, b: {}, c: {}".format(lineBB.a, lineBB.b, lineBB.c))

    # B2: Find I(xI, yI)
    I = Point()
    I = findI(A, B)
    print("I: {}".format([I.x, I.y]))
    ax.scatter(I.x, I.y, c='#8c564b')

    # B3: dmin
    d_min = min(distance(A,I), distance(B,I))
    print("d_min: {}".format(d_min))

    # B4: R
    R = (d_min*math.tan(Phi/2))/2
    print("R calculated: {}".format(R))
    R = R if R < Rmax else Rmax
    R = R if R > Rmin else Rmin
    print("R: {}".format(R))

    # B5: Kiem tra dieu kien
    # vector_AI = [I.x - A.x, I.y - A.y]
    # vector_A = [math.cos(A.phi), math.sin(A.phi)]
    # vector_BI = [I.x - B.x, I.y - B.y]
    # vector_B = [math.cos(B.phi), math.sin(B.phi)]
    # cos_AI_A = cos2Vector(vector_AI, vector_A)
    # cos_IB_B = cos2Vector(vector_BI, vector_B)
    # if(cos_AI_A*cos_IB_B > 0):
    #     print("A, B invalid")
    #     exit()

    # B6: Tim diem Ia, Ib (Diem bat dau va diem ket thuc cung tron)
    # Tim Ia:
    #   + Ia thuoc duong thang qua vecto A
    #   + |IIa| tinh duoc
    #   + cos(vector(IIa), vector(IA)) = 1
    # Ia = findIx(A, R, Phi, I)
    # print("Ia: {}".format([Ia.x, Ia.y]))
    # Ib = findIx(B, R, Phi, I)
    # print("Ib: {}".format([Ib.x, Ib.y]))
    # ax.scatter(Ia.x, Ia.y, color='#8cff00', marker='o')
    # ax.scatter(Ib.x, Ib.y, color='#8cff00')

    # -------------------------------------
    # B7: Chia buoc ve cac diem tren path

    Path = MkacPlanner(A, B, Rmax, Rmin, step)
    # ax.scatter(X, Y, color='g', s=10)
    # show path
    X = []
    Y = []
    U = []
    V = []
    for pose in Path:
        X.append(pose.x)
        Y.append(pose.y)
        U.append(math.cos(pose.phi))
        V.append(math.sin(pose.phi))
    # print("Number of pose in Path: {}".format(len(X)))
    p = ax.quiver(X, Y, U, V, units='xy', scale=0.51 , color='g') #
    ax.scatter(X, Y, color='g', s=10)

    # # show plot
    ax.set_aspect('equal')
    # ax.axline((1,1), (2,2))
    # ax.xlim(Xmin,Xmax)
    # ax.ylim(Ymin,Ymax)
    ax.grid()
    plt.show()

if __name__ == "__main__":
    # A: start, B: stop
    if len(sys.argv) > 1:
        if int(sys.argv[1]) == 1:
            test_invidial()
    else:
        test1()