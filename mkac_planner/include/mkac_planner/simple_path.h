#ifndef _SIMPLE_PATH_H
#define _SIMPLE_PATH_H
#include <vector>
//#include "utils.hpp"

namespace mkac_planner{
struct pose2d
{
  double x = 0.0;
  double y = 0.0;
  double heading = 0.0;
};

struct point2d
{
  double x = 0.0;
  double y = 0.0;
};

struct vector2d
{
  double x = 0.0;
  double y = 0.0;
};

/**
 * @brief line2d: ax + by + c = 0
 */
struct line2d
{
  double a = 0;
  double b = 0;
  double c = 0;
};

class SimplePath{

  public:
	float distance2d(point2d A, point2d B);

	float dot2d(vector2d A, vector2d B);

	float norm2d(vector2d A, vector2d B);

	/**
	 * @brief return cosine similarity of 2 vector 2D
	 */
	float cosine2d(vector2d A, vector2d B);
	line2d lineFromPose(pose2d pose);

	/**
	 * @brief Solve quadratic equation (Giai phuong trinh bac 2): a*x^2 + b*x + c = 0
	 *
	 * @return bool
	 */
//	bool solveQuadraticEq(float a, float b, float c, double& x1, double& x2);
	bool solveQuadraticEq(double a, double b, double c, double& x1, double& x2);

	/**
	 * @brief Find Start pose or stop pose of arc (cung tron)
	 *
	 * @param base
	 * @param R
	 * @param Phi
	 * @param I
	 * @return pose2d
	 */
	pose2d findIx(pose2d base, float R, float Phi, point2d I);


    bool getPath(pose2d start, pose2d stop, float r_max, float r_min, float step, std::vector<pose2d>& path);
    void pathStraight(pose2d start, pose2d stop, float step, std::vector<pose2d>& path);
    void pathArc(pose2d start, pose2d stop, point2d I, float R, float Phi, float step, std::vector<pose2d> &path);
};

}
#endif
