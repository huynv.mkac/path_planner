
#ifndef MKAC_PLANNER_H_
#define MKAC_PLANNER_H_
/*********************************************************************
 *
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2008, 2013, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 * Author: HuyNV-MKAC
 *********************************************************************/
#include <ros/ros.h>
#include <nav_core/base_global_planner.h>
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <nav_msgs/Path.h>
#include <nav_msgs/GetPlan.h>
#include <tf/tf.h>
#include <tf2/convert.h>
#include <tf2/utils.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <math.h>
#include "simple_path.h"
//#include "utils.hpp"

namespace mkac_planner{

class MkacPlanner : public nav_core::BaseGlobalPlanner{
    public:
        /**
         * @brief Default constructor for MkacPlanner class
         */
        MkacPlanner();

        /**
         * @brief Constructor for MkacPlanner Object
         * @param name: the Name of this planner
         * @param costmap_ros: A pointer to the costmap to use
         * @param frame_id Frame of costmap
         */
        MkacPlanner(std::string name, costmap_2d::Costmap2D* costmap, std::string frame_id);

        /**
         * @brief Destroy the Mkac Planner object
         *
         */
        ~MkacPlanner();

        /**
         * @brief Initialization function for Mkac Planner object
         *
         * @param name The name of this planner
         * @param costmap_ros a pointer to the ROS wrapper of costmap to use for planning
         */
        void initialize(std::string name, costmap_2d::Costmap2DROS *costmap_ros);
        void initialize(std::string name, costmap_2d::Costmap2D *costmap, std::string frame_id);

        /**
         * @brief Given a goal pose in the world, compute a plan
         *
         * @param start the start pose
         * @param stop The stop pose
         * @param plan the plan filled by the planner
         * @return true if a valid plan was found, false otherwise
         */
        bool makePlan(  const geometry_msgs::PoseStamped &start,
                        const geometry_msgs::PoseStamped &stop,
                        std::vector<geometry_msgs::PoseStamped>& plan);
        /**
         * @brief Publish a path for visualization purposes
         */
        void publishPlan(const std::vector<geometry_msgs::PoseStamped>& path);

        bool makePlanService(nav_msgs::GetPlan::Request& req, nav_msgs::GetPlan::Response& resp);

        /**
         * @brief Convert from geometry_msgs::PoseStamped to pose2d (x, y, heading)
         */
        pose2d PoseStamped2pose2d(geometry_msgs::PoseStamped pose);

        /**
         * @brief Convert pose2d (x, y, heading) to PoseStamped
         */
        geometry_msgs::PoseStamped pose2d2PoseStamped(pose2d pose2d);
        void pose2d2PoseStamped(pose2d pose2d, geometry_msgs::PoseStamped& pose);


    protected:
        costmap_2d::Costmap2D* costmap_;
        std::string frame_id_;
        ros::Publisher plan_pub_;
        nav_msgs::Path gui_path;

    private:
        bool initialized_;
        ros::ServiceServer make_plan_srv_;
        boost::mutex mutex_;
        float R_min;
        float R_max;
        float step;
        SimplePath *path_maker_;

        void reconfigureCB(mkac_planner::MkacPlannerConfig &config, uint32_t level);
        dynamic_reconfigure::Server<mkac_planner::MkacPlannerConfig> *dsrv_;
};
};
#endif
