 #ifndef _UTILS_H
 #define _UTILS_H

#include <cmath>
#include <iostream>
#include <ros/ros.h>

namespace mkac_planner
{

struct pose2d
{
  double x = 0.0;
  double y = 0.0;
  double heading = 0.0;
};

struct point2d
{
  double x = 0.0;
  double y = 0.0;
};

struct vector2d
{
  double x = 0.0;
  double y = 0.0;
};

/**
 * @brief line2d: ax + by + c = 0
 *
 */
struct line2d
{
  float a = 0;
  float b = 0;
  float c = 0;
};

/**
 * @brief compute distance between two point in 2D
 */
float distance2d(point2d A, point2d B){
  return sqrt(pow((A.x - B.x), 2) + pow((A.y - B.y), 2));
}

float dot2d(vector2d A, vector2d B){
  return (A.x * B.x + A.y * B.y);
}

float norm2d(vector2d A, vector2d B){
  return sqrt(pow(A.x, 2) + pow(A.y, 2)) * sqrt(pow(B.x, 2) + pow(B.y, 2));
}

/**
 * @brief return cosine similarity of 2 vector 2D
 */
float cosine2d(vector2d A, vector2d B){
  return dot2d(A, B) / norm2d(A, B);
}

line2d lineFromPose(pose2d pose){
  line2d line;
  if (pose.heading == M_PI_2 || pose.heading == -M_PI_2)
  {
    line = {1, 0, (float)-pose.x};
  }
  else
  {
    line.a = -tan(pose.heading);
    line.b = 1;
    line.c = (pose.y - tan(pose.heading)*pose.x);
  }
  return line;
}

/**
 * @brief Solve quadratic equation (Giai phuong trinh bac 2): a*x^2 + b*x + c = 0
 *
 * @return bool
 */
bool solveQuadraticEq(float a, float b, float c, double& x1, double& x2){
  float delta = b*b - 4*a*c;
  if (delta < 0)
  {
    return false;
  }
  else{
    x1 = (-b + sqrt(delta))/(2*a);
    x2 = (-b - sqrt(delta))/(2*a);
    return true;
  }

}

/**
 * @brief Find Start pose or stop pose of arc (cung tron)
 *
 * @param base
 * @param R
 * @param Phi
 * @param I
 * @return pose2d
 */
pose2d findIx(pose2d base, float R, float Phi, point2d I){
  pose2d Ix;
  line2d base_line = lineFromPose(base);

  double x1, x2, y1, y2;

  if (base_line.b != 0)
  {
    float a1 = -base_line.a/base_line.b;
    float b1 = -base_line.c/base_line.b;
    float tmp_a = pow(a1, 2) + 1;
    float tmp_b = (-2*I.x - 2*a1*I.y + 2*a1*b1);
    float tmp_c = pow(I.x, 2) + pow((b1 - I.y), 2) - pow(R*tan(Phi/2), 2);

    if (solveQuadraticEq(tmp_a, tmp_b, tmp_c, x1, x2))
    {
      y1 = a1*x1 + b1;
      y2 = a1*x2 + b1;
    }
    else{
      ROS_ERROR("[findIx] cannot solve quadratic equation");
      return Ix;
    }
  }
  else{ // base_line.b == 0
    y1 = I.y + sqrt(pow(R*tan(Phi/2), 2) - pow((-base_line.c/base_line.a - I.x), 2));
    y1 = I.y - sqrt(pow(R*tan(Phi/2), 2) - pow((-base_line.c/base_line.a - I.x), 2));

    x1 = x2 = -base_line.c/base_line.a;
  }

  // Kiem tra dieu kien loai bo
  vector2d vec_I_start = {base.x - I.x, base.y - I.y};
  vector2d vec_I_Ix1 = {x1 - I.x, y1 - I.y};

  if (cosine2d(vec_I_start, vec_I_Ix1) > 0){
    Ix.x = x1;
    Ix.y = y1;
  }
  else{
    Ix.x = x2;
    Ix.y = y2;
  }

  Ix.heading = base.heading;

  return Ix;
}

} // namespace mkac_planner
 #endif
