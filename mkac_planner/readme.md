readme.md
==

Test code cpp
--
Đặt `#define DEBUG 1` trong `simple_path.cpp`.
Mở terminal:
```
cd <path_to_folder>/mkac_planner/src/
g++ -I../include test_lib.cpp simple_path.cpp -o test && ./test
```

Usage 
---

1. clone src code into catkin_ws/src
2. catkin_make
3. set base_local_planner: mkac_planner/MkacPlanner
