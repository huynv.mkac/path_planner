---
Other: HuyNV
Ognization: MKAC
Date: Dec, 2020
---
# Tạo global path plan cho AGF

## Mục tiêu

- Tạo quỹ đạo:
  - Đi thẳng
  - Rẽ theo cung tròn
  - Nối nhiều đoạn thành path đầy đủ cho robot

- Tích hợp thành plugin để move_base điều khiển robot di chuyển theo path đã thiết lập
- Tích hợp các tools vào rviz để thiết lập, tùy chỉnh path

## Tạo quỹ đạo

- (Dec 07, 2020) Tạo được path với:
  - Input: Start pose, Stop pose
  - Output: path với đặc điểm: đi thẳng, rẽ theo cung tròn, đi thẳng tới Stop pose
- Tài liệu, công thức ... (chưa cập nhật)
<!-- TODO: Cập nhật tài liệu các công thức thiết lập path -->

## Tích hợp thành plugin của move_base

- Thử thành công với package: my_global_planner
- Đưa các thông số thiết lập ra Dynamic Reconfigure

## Tools trên Rviz để tạo, tùy chỉnh path

- Yêu cầu
  - Có điểm định nghĩa waypoint, Start, Stop
  - Nối nhiều đường lại với nhau
  - Có thể sửa path được
(Chưa có)

## Todo log:

- Dec 11, 2020:
  - Tạo xong khung plugin
  - Tạo xong Dynamic reconfigure



## Refs:
- DETERMINISTIC PATH PLANNING AND NAVIGATION FOR AN AUTONOMOUS FORK LIFT TRUCK.pdf
- <https://answers.ros.org/question/39119/custom-global-planner/>
- <http://wiki.ros.org/navigation/Tutorials/Writing%20A%20Global%20Path%20Planner%20As%20Plugin%20in%20ROS>
- <http://wiki.ros.org/dynamic_reconfigure/Tutorials>