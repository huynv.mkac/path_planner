#include "mkac_planner/simple_path.h"
#include <stdlib.h>
#include <cmath>
#include <vector>

#define DEBUG 0
#if DEBUG
#include <iostream>
#else
#include <ros/ros.h>
#endif

namespace mkac_planner{

float SimplePath::distance2d(point2d A, point2d B){
  return (float)sqrt(pow((A.x - B.x), 2) + pow((A.y - B.y), 2));
//	return 1;
}

float SimplePath::dot2d(vector2d A, vector2d B){
  return (float)(A.x * B.x + A.y * B.y);
}

float SimplePath::norm2d(vector2d A, vector2d B){
  return (float)sqrt(pow(A.x, 2) + pow(A.y, 2)) * sqrt(pow(B.x, 2) + pow(B.y, 2));
}

float SimplePath::cosine2d(vector2d A, vector2d B){
  return dot2d(A, B) / norm2d(A, B);
}

line2d SimplePath::lineFromPose(pose2d pose){
  line2d line;
  if (pose.heading == M_PI_2 || pose.heading == -M_PI_2)
  {
    line = {1, 0, (float)(-pose.x)};
  }
  else
  {
    line.a = - tan(pose.heading);
    line.b = 1.0;
    line.c = - (pose.y - (tan(pose.heading))*pose.x);
  }
  return line;
}

bool SimplePath::solveQuadraticEq(double a, double b, double c, double& x1, double& x2){
  double delta = b*b - 4*a*c;
  if (delta < 0)
  {
#if DEBUG
    std::cout << "Delta < 0: " << delta << std::endl;
#endif
    return false;
  }
  else{
    x1 = (double)(-b + sqrt(delta))/(2*a);
    x2 = (double)(-b - sqrt(delta))/(2*a);
    return true;
  }

}

pose2d SimplePath::findIx(pose2d base, float R, float Phi, point2d I){
  pose2d Ix;
  line2d base_line = lineFromPose(base);
#if DEBUG
  std::printf("line: [%f, %f, %f]\n", base_line.a, base_line.b, base_line.c);
#endif
  double x1, x2, y1, y2;

  if (base_line.b != 0)
  {
    double a1 = -base_line.a/base_line.b;
    double b1 = -base_line.c/base_line.b;
    double tmp_a = pow(a1, 2) + 1;
    double tmp_b = (double)((-2*I.x - 2*a1*I.y + 2*a1*b1));
    double tmp_c = (double)(pow(I.x, 2) + pow((b1 - I.y), 2) - pow(R*tan(Phi/2), 2));

#if DEBUG
      std::printf("Ptb2: a: %f, b: %f, c: %f\n", tmp_a, tmp_b, tmp_c);
#endif
    if (solveQuadraticEq(tmp_a, tmp_b, tmp_c, x1, x2))
    {
      y1 = a1*x1 + b1;
      y2 = a1*x2 + b1;
#if DEBUG
      std::printf("x1: %f, y1: %f\n", x1, y1);
      std::printf("x2: %f, y2: %f\n", x2, y2);
#endif
    }
    else{
#if DEBUG
    std::cout << "[findIx] cannot solve quadratic equation" << std::endl;
#else
    ROS_ERROR("[findIx] cannot solve quadratic equation");
#endif
      return Ix;
    }
  }
  else{ // base_line.b == 0
    y1 = I.y + sqrt(pow(R*tan(Phi/2), 2) - pow((-base_line.c/base_line.a - I.x), 2));
    y1 = I.y - sqrt(pow(R*tan(Phi/2), 2) - pow((-base_line.c/base_line.a - I.x), 2));

    x1 = x2 = -base_line.c/base_line.a;
  }

  // Kiem tra dieu kien loai bo
  vector2d vec_I_start = {base.x - I.x, base.y - I.y};
  vector2d vec_I_Ix1 = {x1 - I.x, y1 - I.y};

  if (cosine2d(vec_I_start, vec_I_Ix1) > 0){
    Ix.x = x1;
    Ix.y = y1;
#if DEBUG
    std::cout << "cosine > 0 | " << cosine2d(vec_I_start, vec_I_Ix1) << std::endl;
    std::printf("x1: %f, y1: %f\n", x1, y1);
#endif
  }
  else{
    Ix.x = x2;
    Ix.y = y2;
  }

  Ix.heading = base.heading;

  return Ix;
}

bool SimplePath::getPath(pose2d start, pose2d stop, float r_max, float r_min, float step, std::vector<pose2d>& path){
  point2d pointStart = {start.x, start.y};
  point2d pointStop = {stop.x, stop.y};

  // B1: Phi
  float Phi = abs(start.heading - stop.heading);
  if (Phi > M_PI){
    Phi = 2*M_PI - Phi;
  }
#if DEBUG
  std::cout << "Phi: " << Phi << std::endl;
#endif

//  return true;

  // B2: find intersection I
  point2d I;
  float a1 = tan(start.heading);
  float a2 = tan(stop.heading);
  float b1 = start.y - tan(start.heading)*start.x;
  float b2 = stop.y - tan(stop.heading)*stop.x;

  I.x = (b2 - b1)/(a1 - a2);
  I.y = (a1*b2 - a2*b1)/(a1-a2);
#if DEBUG
	std::cout << "Giao diem I: " << I.x << "; " << I.y << std::endl;
#endif

	// Step 3: find d_min
  float d_min = std::min(distance2d(pointStart, I), distance2d(I, pointStop));
#if DEBUG
  std::cout << "d_min: " << d_min << std::endl;
#endif

  // Step 4: find turning radius
  float R = (d_min*tan(Phi/2))/2;
  if (R > r_max){ R = r_max;}
  if (R < r_min){ R = r_min;}
#if DEBUG
  std::cout << "R: " <<  R << std::endl;
#endif

  // Step 5: Check condition
  vector2d vec_start_I = {I.x - start.x, I.y - start.y};
  vector2d vec_start = {cos(start.heading), sin(start.heading)};
  vector2d vec_stop_I = {I.x - stop.x, I.y - stop.y};
  vector2d vec_stop = {cos(stop.heading), sin(stop.heading)};

  if (cosine2d(vec_start_I, vec_start) * cosine2d(vec_stop_I, vec_stop) > 0) {
#if DEBUG
	  std::cout << "Start and Stop pose cannot create path!" << std::endl;
#else
    ROS_WARN("Start and Stop pose cannot create path!");
#endif
    return false;
  }

  // Step 6: Find Start and stop point of arc (Ia, Ib)
  pose2d Ia = findIx(start, R, Phi, I);
  pose2d Ib = findIx(stop, R, Phi, I);
#if DEBUG
  std::printf("Ia: %f; %f \nIb: %f; %f\n", Ia.x, Ia.y, Ib.x, Ib.y);
//  std::cout << ("Ia: %.2f; %.2f \nIb: %.2f; %.2f", Ia.x, Ia.y, Ib.x, Ib.y) << std::endl;
#endif

  std::vector<pose2d> path1, path2, path3;

  pathStraight(start, Ia, step, path1);
  pathArc(Ia, Ib, I, R, Phi, step, path2);
  pathStraight(Ib, stop, step, path3);


  path.insert(path.end(), path1.begin(), path1.end());
  path.insert(path.end(), path2.begin(), path2.end());
  path.insert(path.end(), path3.begin(), path3.end());

#if DEBUG
  std::cout << "Path 1 straight have: " << path1.size() << " poses" << std::endl;
  std::cout << "Path 2 arc have: " << path2.size() << " poses" << std::endl;
  std::cout << "Path 3 straight have: " << path3.size() << " poses" << std::endl;
  std::cout << "Path created have: " << path.size() << " poses" << std::endl;
#endif

  return true;
};

void SimplePath::pathStraight(pose2d start, pose2d stop, float step, std::vector<pose2d> &path){
  float phi = start.heading;
  float x0 = start.x;
  float y0 = start.y;
  point2d startPoint = {start.x, start.y};
  point2d stopPoint = {stop.x, stop.y};

  int numberStep = (int)(distance2d(startPoint, stopPoint) / step);
#if DEBUG
  std::cout << "[pathStraight] numberStep: " << numberStep << std::endl;
#endif
  float x1 = x0 + step*cos(phi);
  float y1 = y0 + step*sin(phi);

  vector2d vec_start_P = {x1 - x0, y1 - y0};
  vector2d vec_start_stop = {stopPoint.x - x0, stopPoint.y - y0};
  if (cosine2d(vec_start_P, vec_start_stop) > 0)
  {
    for (size_t i = 0; i < numberStep; i++)
    {
      x1 = x0 + step*cos(phi);
      y1 = y0 + step*sin(phi);
      path.push_back({x1, y1, phi});
      x0 = x1;
      y0 = y1;
    }
  }
  else{
	  for (size_t i = 0;  i < numberStep; i++) {
		  x1 = x0 - step*cos(phi);
		  y1 = y0 - step*sin(phi);
		  path.push_back({x1, y1, phi});
		  x0 = x1;
		  y0 = y1;
	  }
  }
};

void SimplePath::pathArc(pose2d start, pose2d stop, point2d I, float R, float Phi, float step, std::vector<pose2d>& path){
  float phi0 = start.heading;
  float x0 = start.x;
  float y0 = start.y;

  float stepAngular = step/R;
  int numberStep = (int) (Phi/stepAngular);
#if DEBUG
  std::cout << "[pathArc] Phi: " << Phi << " |stepAngular: " << stepAngular << " | numberStep: " << numberStep << std::endl;
#endif

  float x1 = x0 + step * cos(phi0 + stepAngular/2);
  float y1 = y0 + step * sin(phi0 + stepAngular/2);
  float phi1;

  line2d lineStart = lineFromPose(start);
  float side_P_I = (lineStart.a*x1 + lineStart.b*y1 + lineStart.c) * (lineStart.a * stop.x + lineStart.b * stop.y + lineStart.c);
  int signStep = (side_P_I > 0) ? 1 : -1; // = 1 neu side_P_I > 0, = -1 otherwise

  vector2d vec_start_P = {x1 - start.x, y1 - start.y};
  vector2d vec_start_I = {I.x - start.x, I.y - start.y};

  if (cosine2d(vec_start_P, vec_start_I) > 0)
  {
    for (int i = 0; i < numberStep; i++)
    {
      x1 = x0 + step * cos(phi0 + signStep*stepAngular/2);
      y1 = y0 + step * sin(phi0 + signStep*stepAngular/2);
      phi1 = phi0 + signStep*stepAngular;
      path.push_back({x1, y1, phi1});
      x0 = x1;
      y0 = y1;
      phi0 = phi1;
    }
  }
  else{
    signStep = -signStep;
    for (int i = 0; i < numberStep; i++)
    {
      x1 = x0 - step * cos(phi0 + signStep*stepAngular/2);
      y1 = y0 - step * sin(phi0 + signStep*stepAngular/2);
      phi1 = phi0 + signStep*stepAngular;
      path.push_back({x1, y1, phi1});
      x0 = x1;
      y0 = y1;
      phi0 = phi1;
    }
  }
}

};
