/*
 * test_lib.cpp
 *
 *  Created on: Dec 14, 2020
 *      Author: huy
 */

#include "mkac_planner/simple_path.h"
#include <iostream>
#include <vector>
using namespace mkac_planner;
using namespace std;
int main(){
	std::cout << "TEST" << std::endl;

	pose2d B = {1.56, 0, 3.14};
	pose2d A = {-7.5, -5.5, -1.57};

	SimplePath simplePath;
	std::vector<pose2d> path;

	bool x = simplePath.getPath(A, B, 2, 0.5, 0.2, path);
	std::cout << "getPath: "<< x << std::endl;

	return 0;
}



