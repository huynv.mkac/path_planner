#include <pluginlib/class_list_macros.h>
#include <dynamic_reconfigure/server.h>
#include <mkac_planner/MkacPlannerConfig.h>
#include "mkac_planner/mkac_planner.h"
//#include <nav_msgs/Path.h>
// #include "../include/mkac_planner/mkac_planner.h"

PLUGINLIB_EXPORT_CLASS(mkac_planner::MkacPlanner, nav_core::BaseGlobalPlanner)

using namespace std;

namespace mkac_planner{

MkacPlanner::MkacPlanner() : initialized_(false), costmap_(NULL){}

MkacPlanner::MkacPlanner(std::string name, costmap_2d::Costmap2D* costmap, std::string frame_id)
: MkacPlanner(){
  initialize(name, costmap, frame_id);
}

MkacPlanner::~MkacPlanner(){
	delete path_maker_;
}

void MkacPlanner::initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros){
  initialize(name, costmap_ros->getCostmap(), costmap_ros->getGlobalFrameID());
}

void MkacPlanner::initialize(std::string name, costmap_2d::Costmap2D* costmap, std::string frame_id){
  if(!initialized_){
    ros::NodeHandle private_nh("~/" + name);
//    ROS_INFO("init MkacPlanner");
    costmap_ = costmap;
    frame_id_ = frame_id;

    make_plan_srv_ = private_nh.advertiseService("make_plan", &MkacPlanner::makePlanService, this);
    plan_pub_ = private_nh.advertise<nav_msgs::Path>("plan", 1);
    path_maker_ = new SimplePath();

    private_nh.param("turning_min_radius", R_min, (float)0.5);
    private_nh.param("turning_max_radius", R_max, (float)2.0);
    private_nh.param("step_distance", step, (float)0.2);

    // Dynamic reconfigure
    dsrv_ = new dynamic_reconfigure::Server<mkac_planner::MkacPlannerConfig>(ros::NodeHandle("~/" + name));
    dynamic_reconfigure::Server<mkac_planner::MkacPlannerConfig>::CallbackType f;
    f = boost::bind(&MkacPlanner::reconfigureCB, this, _1, _2);
    dsrv_->setCallback(f);

    initialized_ = true;
  }
}

void MkacPlanner::reconfigureCB(mkac_planner::MkacPlannerConfig &config, uint32_t level){
  ROS_DEBUG("Update reconfigure.");
  R_min = config.turning_min_radius;
  R_max = config.turning_max_radius;
  step = config.step_distance;
}

bool MkacPlanner::makePlanService(nav_msgs::GetPlan::Request& req, nav_msgs::GetPlan::Response& resp){
  makePlan(req.start, req.goal, resp.plan.poses);

  resp.plan.header.stamp = ros::Time::now();
  resp.plan.header.frame_id = frame_id_;

  return true;
}

bool MkacPlanner::makePlan(const geometry_msgs::PoseStamped& start,
                            const geometry_msgs::PoseStamped& goal,
                            std::vector<geometry_msgs::PoseStamped>& plan){
  boost::mutex::scoped_lock lock(mutex_);
  if(!initialized_){
    ROS_ERROR("This planner has not been initialized yet, please call initialize() before use");
    return false;
  }

  plan.clear();

  ros::NodeHandle n;
  std::string global_frame = frame_id_;
  if(goal.header.frame_id != global_frame){
    ROS_ERROR(
      "The goal pose passed to this planner must be in %s frame. "
      "It is instead in the %s frame.", global_frame.c_str(), goal.header.frame_id.c_str());
      return false;
  }

  if (start.header.frame_id != global_frame){
    ROS_ERROR(
      "The start pose passed to this planner must be in %s frame. "
      "It is instead in the %s frame.", global_frame.c_str(), start.header.frame_id.c_str());
  }

  pose2d pose2dStart, pose2dGoal;

//  pose2dStart = PoseStamped2pose2d(start);
//  pose2dGoal = PoseStamped2pose2d(goal);

  pose2dStart.x = start.pose.position.x;
  pose2dStart.y = start.pose.position.y;
  pose2dStart.heading = tf2::getYaw(start.pose.orientation);

  pose2dGoal.x = goal.pose.position.x;
  pose2dGoal.y = goal.pose.position.y;
  pose2dGoal.heading = tf2::getYaw(goal.pose.orientation);


  ROS_INFO("Get path: Start(%f, %f, %f) - Goal(%f, %f, %f)",
		  pose2dStart.x, pose2dStart.y, pose2dStart.heading,
		  pose2dGoal.x, pose2dGoal.y, pose2dGoal.heading);

  std::vector<pose2d> path;

  path.push_back(pose2dStart);
#define TEST 0
#if !TEST
  if(!path_maker_->getPath(pose2dStart, pose2dGoal, R_max, R_min, step, path)){
	  ROS_ERROR("NO PATH!");
	  return false;
  }
#endif
  path.push_back(pose2dGoal);
  ros::Time plan_time = ros::Time::now();
  for (int i = 0; i < path.size(); i++){
	  geometry_msgs::PoseStamped pose;

	  pose.header.stamp = plan_time;
	  pose.header.frame_id = global_frame;
//	  pose2d2PoseStamped(path[i], pose);

	  tf2::Quaternion quat;
	  quat.setRPY(0, 0, path[i].heading);
	  pose.pose.position.x = path[i].x;
	  pose.pose.position.y = path[i].y;

	  pose.pose.orientation.x = quat.x();
	  pose.pose.orientation.y = quat.y();
	  pose.pose.orientation.z = quat.z();
	  pose.pose.orientation.w = quat.w();
	  plan.push_back(pose);
  }
  publishPlan(plan);
  return !plan.empty();
}

//bool MkacPlanner::makePlan(const geometry_msgs::PoseStamped& start,
//                            const geometry_msgs::PoseStamped& goal,
//                            std::vector<geometry_msgs::PoseStamped>& plan){
//	ROS_INFO("Make plan!");
//	return true;
//}

void MkacPlanner::publishPlan(const std::vector<geometry_msgs::PoseStamped>& path){
	if(!initialized_){
		ROS_ERROR("This planner has not been initialized yet!");
		return;
	}
//	nav_msg::Path gui_path;
	gui_path.poses.resize(path.size());
	gui_path.header.frame_id = frame_id_;
	gui_path.header.stamp = ros::Time::now();

	for(unsigned int i=0; i < path.size(); i++){
		gui_path.poses[i] = path[i];
	}
	plan_pub_.publish(gui_path);
}

pose2d MkacPlanner::PoseStamped2pose2d(geometry_msgs::PoseStamped pose){
	pose2d pose2d;
	pose2d.x = pose.pose.position.x;
	pose2d.y = pose.pose.position.y;

	tf::Quaternion q(
			pose.pose.orientation.x,
			pose.pose.orientation.y,
			pose.pose.orientation.z,
			pose.pose.orientation.w);
	double roll, pitch, yaw;
	tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
	pose2d.heading = yaw;
	return pose2d;
}

geometry_msgs::PoseStamped MkacPlanner::pose2d2PoseStamped(pose2d pose2d){
	geometry_msgs::PoseStamped pose;

	pose.pose.position.x = pose2d.x;
	pose.pose.position.y = pose2d.y;
	pose.pose.position.z = 0;

	tf::Quaternion quat;
	quat.setRPY(0, 0, pose2d.heading);
	pose.pose.orientation.x = quat[0];
	pose.pose.orientation.y = quat[1];
	pose.pose.orientation.z = quat[2];
	pose.pose.orientation.w = quat[3];

	return pose;
}

void MkacPlanner::pose2d2PoseStamped(pose2d pose2d, geometry_msgs::PoseStamped& pose){
	pose.pose.position.x = pose2d.x;
	pose.pose.position.y = pose2d.y;
	pose.pose.position.z = 0.0;

	tf::Quaternion quat;
	quat.setRPY(0, 0, pose2d.heading);
	pose.pose.orientation.x = quat[0];
	pose.pose.orientation.y = quat[1];
	pose.pose.orientation.z = quat[2];
	pose.pose.orientation.w = quat[3];
}
};

