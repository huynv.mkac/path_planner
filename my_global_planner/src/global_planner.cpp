#include <pluginlib/class_list_macros.h>
#include "global_planner.h"
#include <dynamic_reconfigure/server.h>
#include <my_global_planner/MyGlobalPlannerConfig.h> // generated from MyGlobalPlanner.cfg file

// Dang ki day la planner nhu mot BaseGlobalPlanner plugin
PLUGINLIB_EXPORT_CLASS(my_global_planner::MyGlobalPlanner, nav_core::BaseGlobalPlanner)

using namespace std;

// Ham dung mac dinh
namespace my_global_planner{

MyGlobalPlanner::MyGlobalPlanner()
: initialized_(false){}

MyGlobalPlanner::MyGlobalPlanner(std::string name, costmap_2d::Costmap2DROS* costmap_ros)
: initialized_(false){
    initialize(name, costmap_ros);
}

void MyGlobalPlanner::reconfigureCB(my_global_planner::MyGlobalPlannerConfig &config, uint32_t level){
    ROS_INFO("Reconfigure: %d", config.num_step);
    num_step = config.num_step;
}

void MyGlobalPlanner::initialize(std::string name, costmap_2d::Costmap2DROS* costmap_ros){
    if(!initialized_){
        ros::NodeHandle private_nh("~/" + name);
        private_nh.param("num_step", num_step, 20);

        // Dynamic_reconfigure
        dsrv_ = new dynamic_reconfigure::Server<my_global_planner::MyGlobalPlannerConfig>(ros::NodeHandle("~/" + name));
        dynamic_reconfigure::Server<my_global_planner::MyGlobalPlannerConfig>::CallbackType f;
        f = boost::bind(&MyGlobalPlanner::reconfigureCB, this, _1, _2);
        dsrv_->setCallback(f);

        initialized_ = true;
    }
}

/** Tao plan
 *  Tinh toan cac diem lien ke tu diem start den diem goal
 *  push_back cac diem trong plan vao vector plan
 **/
bool MyGlobalPlanner::makePlan(   const geometry_msgs::PoseStamped& start,
                                const geometry_msgs::PoseStamped& goal,
                                std::vector<geometry_msgs::PoseStamped>& plan){
    plan.push_back(start);
    // ros::NodeHandle private_nh("~/" + name);
    // private_nh.param("num_step", num_step, 20);
    ROS_INFO("Numstep: %d", num_step);
    for (int i = 0; i < num_step; i++)
    {
        geometry_msgs::PoseStamped new_goal = goal;
        tf::Quaternion goal_quat = tf::createQuaternionFromYaw(1.54);

        new_goal.pose.position.x = start.pose.position.x + (0.05*i);
        new_goal.pose.position.y = start.pose.position.y + (0.05*i);

        new_goal.pose.orientation.x = goal_quat.x();
        new_goal.pose.orientation.y = goal_quat.y();
        new_goal.pose.orientation.z = goal_quat.z();
        new_goal.pose.orientation.w = goal_quat.w();

        plan.push_back(new_goal);
    }

    plan.push_back(goal);
    return true;
}
};

